import React from 'react'
import Register from '../../Components/Company/Register/Register'
import Nav from '../../Components/User/Nav/Nav'

function CompanyRegister() {
  return (
    <div>
        <Nav/>
      <Register/>
    </div>
  )
}

export default CompanyRegister
