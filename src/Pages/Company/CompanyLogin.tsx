import React from 'react'
import Nav from '../../Components/User/Nav/Nav'

import Clogin from '../../Components/Company/Login/Clogin'

function CompanyLogin() {
  return (
    <div>
      <Nav/>
       <Clogin/>
    </div>
  )
}

export default CompanyLogin
