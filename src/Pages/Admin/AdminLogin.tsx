import React from 'react'
import Alogin from '../../Components/Admin/Alogin'
import Nav from '../../Components/User/Nav/Nav'

function AdminLogin() {
  return (
    <div>
        <Nav/>
      <Alogin/>
    </div>
  )
}

export default AdminLogin
